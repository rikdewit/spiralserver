# Setup development environment
Execute the following:

    docker-compose build main
    touch DEVELOP
    touch .env
    echo "LEDENBASE_AUTH_TOKEN=<LEDENBASE_AUTH_TOKEN>" >> .env
    echo "LEDENBASE_BASE_URL=https://theta.ledenbase.nl" >> .env
    mkdir database
    ./db.sh
    docker-compose build main
    docker-compose up main

Get <LEDENBASE_AUTH_TOKEN> from https://theta.ledenbase.nl/common/clientapplication/

# Documentation
Django:
  - https://docs.djangoproject.com/en/dev/
  - http://django-crispy-forms.readthedocs.io/en/latest/
  - http://django-ckeditor.readthedocs.io/en/latest/
  - https://github.com/asaglimbeni/django-datetime-widget

Bootstrap:
  - http://getbootstrap.com/components/
  - https://www.w3schools.com/bootstrap/
  - http://django-bootstrap3.readthedocs.io/

Dokku:
  - http://dokku.viewdocs.io/dokku/

# Deploying
## Initial deploy
Add the following code to your SSH config file. To be able to push applications to the deploymentserver it needs to be aware of your public SSH key.

    Host <ssh host>
      HostName <ip address deploymentserver>
      Port 22
      User dokku
      RequestTTY yes
      ForwardAgent yes

Add a repository named <app name> to a remote named <ssh host>.

    git remote add <remote name> <ssh host>:<app name>

Example:

    git remote add metis metis:botenwagen

Create the application on the deploymentserver:

    ssh <ssh host> apps:create <app name>

Inject secret keys and configuration parameters with environment variables:

    ssh <ssh host> config:set <app name> LEDENBASE_BASE_URL=https://theta.ledenbase.nl LEDENBASE_AUTH_TOKEN=<LEDENBASE_AUTH_TOKEN>

Set up shared folders. The first path designates the path on the host and the second path indicates the path within the container.

    ssh <ssh host> docker-options:add <app name> deploy "-v /home/<app name>/database:/code/database"
    ssh <ssh host> docker-options:add <app name> deploy "-v /home/<app name>/staticfiles:/code/staticfiles"
    ssh <ssh host> docker-options:add <app name> deploy "-v /home/<app name>/mediafiles:/code/mediafiles"
    ssh <ssh host> docker-options:add <app name> run "-v /home/<app name>/database:/code/database"
    ssh <ssh host> docker-options:add <app name> run "-v /home/<app name>/staticfiles:/code/staticfiles"
    ssh <ssh host> docker-options:add <app name> run "-v /home/<app name>/mediafiles:/code/mediafiles"

Deploy the application:

    git push <remote name> <deploy branch>:master

Optional: add extra domains to the application (depending on your DNS settings):

    ssh <ssh host> domains:add <app name> <domain>

Enable TLS with an SSL-certificate using dokku's letsencrypt plugin (make sure the DNS settings are correct and active, otherwise this will not work):

    ssh <ssh host> config:set <app name> DOKKU_LETSENCRYPT_EMAIL=webmaster@esrtheta.nl
    ssh <ssh host> letsencrypt <app name>

Initialize the database on the deploymentserver:

    ssh <ssh host> run <app name> python manage.py migrate --noinput

Let the application collect all the staticfiles (they will be stored in '/home/<app name>/staticfiles' on the host):

    ssh <ssh host> run <app name> python manage.py collectstatic --noinput

Configure the nginx proxy to serve the static and mediafiles. These commands need to be run on the server, so you need root access to the deploymentserver.

    mkdir /home/dokku/<app name>/nginx.conf.d/
    chown -R dokku:dokku /home/dokku/<app name>/nginx.conf.d/

The contents of 'extra.conf':

    client_max_body_size 5m;
    location /mediafiles {
      alias /home/<app name>/mediafiles;
    }
    location /staticfiles {
      alias /home/<app name>/staticfiles;
    }

Reload nginx for the changes to have effect:

    service nginx reload

Set up a connection with an email service, e.g. fastmail.com:

    ssh <ssh host> config:set <app name> EMAIL_HOST=smtp.fastmail.com EMAIL_HOST_USER=schild@esrtheta.nl EMAIL_HOST_PASSWORD=<FASTMAIL_SMTP_PASSWORD>


## Re-deploy
Whenever a new version needs to be deployed:

    git push <remote name> <branch>:master

If you have changed the models and added migrations to the repository:

    ssh <ssh host> run <app name> python manage.py migrate

If you have changed static files:
  
    ssh <ssh host> run <app name> python manage.py collectstatic --noinput
