from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from django.contrib.auth.models import User



class Score(models.Model):
    user = models.ForeignKey(User,on_delete=models.PROTECT)
    time = models.FloatField()
    #HighScore = models.ForeignKey(HighScore,related_name='scores',on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return "{}|{}".format(self.user,self.time)

    class Meta:
        verbose_name = 'score'
        verbose_name_plural = 'scores'
        ordering = ['time']

class Sessie(models.Model):
    id = models.IntegerField(
        primary_key=True,
        unique=True,
    )

    score = models.ManyToManyField(Score)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


    def __str__(self):
        return "{}|{}".format(self.id,self.created_at.strftime("%A-%d-%m-%Y-%-H:%-M"))

    class Meta:
        verbose_name = 'sessie'
        verbose_name_plural = 'sessies'


class SingletonModel(models.Model):
    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.pk = 1
        super(SingletonModel, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        pass

    @classmethod
    def load(cls):
        obj, created = cls.objects.get_or_create(pk=1)
        return obj

class HighScore(SingletonModel):
    score = models.ManyToManyField(Score,blank=True,related_name="highscore")
    def __str__(self):
        return "Highscores"


    class Meta:
        verbose_name = "HighScore"
