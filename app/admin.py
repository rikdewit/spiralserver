from django.contrib import admin
from .models import Sessie, Score, HighScore

admin.site.register(Sessie)
admin.site.register(Score)
admin.site.register(HighScore)
