from django.urls import path, re_path, include
from django.conf import settings
from django.views.generic.base import RedirectView

from app import views

from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'highscore', views.HighScoreViewSet)
router.register(r'score', views.ScoreViewSet)

urlpatterns = [
    path('', views.highscoreRedirect.as_view()),
    path('api/',include(router.urls)),
    #path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('highscores/<int:pk>', views.HighScoreView.as_view(), name="highscores")

    #re_path(r'^sessie/(?P<id>[0-9]+)/$',views.SessieView.as_view(),name='newsitem'),
]
