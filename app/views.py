from django.shortcuts import render, redirect
from django.views import View
from django.views.generic import ListView, DetailView, RedirectView
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import get_object_or_404
from django.core.exceptions import PermissionDenied

from datetime import datetime

from .models import Sessie, HighScore, Score

from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework.response import Response
from .serializers import UserSerializer, HighScoreSerializer, ScoreSerializer


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer

class HighScoreViewSet(viewsets.ModelViewSet):
    queryset = HighScore.objects.all()
    serializer_class = HighScoreSerializer

class ScoreViewSet(viewsets.ModelViewSet):
    queryset = Score.objects.all()
    serializer_class = ScoreSerializer

    def list(self, request):
        queryset = Score.objects.all()
        serializer = ScoreSerializer(queryset, many=True)
        return Response(serializer.data)


class HighScoreView(DetailView):
    model = HighScore
    template_name = "app/highscores.html"

    def get_context_data(self, **kwargs):
        context = super(HighScoreView, self).get_context_data(**kwargs)
        context['scores'] = HighScore.objects.get(pk=1).score.all()
        return context

class highscoreRedirect(RedirectView):
    url = "highscores/1"
