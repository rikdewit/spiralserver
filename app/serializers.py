from django.contrib.auth.models import User
from .models import HighScore, Score
from rest_framework import serializers

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['username']


class ScoreSerializer(serializers.ModelSerializer):

    highscore = serializers.SlugRelatedField(many=True, slug_field='id',queryset=HighScore.objects.all())
    class Meta:
        model = Score
        fields = ['id','user','time','highscore']


class HighScoreSerializer(serializers.ModelSerializer):


    class Meta:
        model = HighScore
        fields = ['score']
        depth = 1
