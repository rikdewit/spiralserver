DEBUG = True

CRISPY_FAIL_SILENTLY = False

SECRET_KEY = 'MegaSpiraal!'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
   }
}
