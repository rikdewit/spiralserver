rm database/spiraaldatabase.db

find . -name migrations -type d -print0|xargs -0 rm -r --

docker-compose run main python manage.py makemigrations app

docker-compose run main python manage.py migrate --noinput

docker-compose run main python manage.py createsuperuser
#docker-compose run main python manage.py loaddata testdata.json

docker rm -v $(docker ps -a -q -f status=exited)

sudo chown -R $USER:$USER .
